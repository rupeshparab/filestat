# README #

## FileStat ##

This repository is my solution to the following problem statement:


```
Word Count

Build a web application which will ask the user to upload a text file and then show the number of lines, words and characters in that file in a tabular format.
```


## Note ##

Th project has been hosted on [Heroku at https://filestat.herokuapp.com/](https://filestat.herokuapp.com/)

## Features ##
1. Displays stats of the selected file
2. File selection by drag and drop or dialog box
3. Upload file and get a sharable link
4. Throttles max file shares by remote ip of users
5. Rejects files greater than 2MB

## Credits ##
1. [Rails 5](https://github.com/rails/rails)
2. [Bootstrap](https://github.com/twbs/bootstrap)
3. [JQuery](https://github.com/jquery/jquery)
4. [bootstrap-fileinput](https://github.com/kartik-v/bootstrap-fileinput)